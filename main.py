import pandas as pd
import numpy as np
import seaborn as sb
import matplotlib.pyplot as plt

##### Links
# https://allysonf.medium.com/predict-red-wine-quality-with-svc-decision-tree-and-random-forest-24f83b5f3408
# https://www.analyticsvidhya.com/blog/2021/04/wine-quality-prediction-using-machine-learning/
# https://machinelearningprojects.net/wine-quality-prediction/#Step_1_-_Importing_libraries_required_for_Wine_Quality_Prediction
# https://medium.com/@hjhuney/implementing-a-random-forest-classification-model-in-python-583891c99652
# https://data36.com/random-forest-in-python/


### Dataset link
wine = pd.read_csv('winequality-red.csv', encoding="utf-8", delimiter=";")

print('wine.head()', wine.head())

wine.loc[1:, :].astype(float).hist(bins=25,figsize=(10,10))

## display histogram
# plt.show()

plt.figure(figsize=[10,6])
# plot bar graph
plt.bar(wine['quality'],wine['alcohol'],color='red')
# label x-axis
plt.xlabel('quality')
#label y-axis
plt.ylabel('alcohol')
# plt.show()

# ploting heatmap
plt.figure(figsize=[19,10],facecolor='blue')
sb.heatmap(wine.corr(),annot=True)
plt.show()


for a in range(len(wine.corr().columns)):
    for b in range(a):
        if abs(wine.corr().iloc[a,b]) >0.7:
            name = wine.corr().columns[a]
            print(name)

new_wine=wine.drop('total sulfur dioxide',axis=1)

new_wine.isnull().sum()

new_wine.update(new_wine.fillna(new_wine.mean()))

# catogerical vars 
next_wine = pd.get_dummies(new_wine,drop_first=True)

# display new dataframe
next_wine["best quality"] = [ 1 if x>=7 else 0 for x in wine.quality] 
print("next_wine", next_wine)


# splitting dataset
from sklearn.model_selection import train_test_split
x, y = next_wine.loc[:, "fixed acidity": "quality"], next_wine.loc[:, "best quality"]
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33, random_state=66)


### Normalization
#importing module
from sklearn.preprocessing import MinMaxScaler

# creating normalization object 
norm = MinMaxScaler()

# fit data
norm_fit = norm.fit(x_train)
# new_xtrain = norm_fit.transform(x_train)
# new_xtest = norm_fit.transform(x_test)

new_xtrain = x_train
new_xtest = x_test

# display values
print(new_xtrain)


### Applying model
# importing modules
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, mean_squared_error, accuracy_score

#creating RandomForestClassifier constructor
rnd = RandomForestClassifier()

# fit data
rnd.fit(new_xtrain, y_train)

y_predict = rnd.predict(new_xtest)

# predicting score
rnd_score = rnd.score(new_xtest, y_test)
print('score of model is : ', rnd_score)



# display error rate
print('calculating the error')

# calculating mean squared error
MSE = mean_squared_error(y_test, y_predict)

# calculating root mean squared error
RMSE = np.sqrt(MSE)

# display MSE
print('mean squared error is : ', MSE)

# display RMSE
print('root mean squared error is : ', RMSE)
print(classification_report(y_predict, y_test))

acc_rf = accuracy_score(y_test, y_predict)
print('Accuracy = ', acc_rf)
